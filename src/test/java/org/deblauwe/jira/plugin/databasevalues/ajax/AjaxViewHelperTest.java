package org.deblauwe.jira.plugin.databasevalues.ajax;

import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.velocity.DefaultVelocityManager;

import org.deblauwe.jira.plugin.databasevalues.DatabaseRowCache;
import org.deblauwe.jira.plugin.databasevalues.DatabaseValuesViewHelper;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class AjaxViewHelperTest
{


	@Test
	public void testGetHtmlForAjaxResults() throws ClassNotFoundException
	{
		Class.forName( "org.hsqldb.jdbcDriver" );
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb" );
		addDemoData( parameters );


		parameters.setSqlQuery( "select id, firstname, lastname, city, country from customer" );
		parameters.setRenderingEditPattern( "{1} {2}" );
		AjaxViewHelper helper = new AjaxViewHelper( parameters, new DatabaseRowCache( parameters ), new DefaultVelocityManager() );
		String s = helper.getHtmlForAjaxResults( null, "TST", new MockI18nHelper(), AjaxViewResultsPurpose.EDIT );
		Assert.assertEquals( "[" + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Wim Deblauwe\", \"value\": \"1\", \"inputlabel\": \"Wim Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Victor Deblauwe\", \"value\": \"2\", \"inputlabel\": \"Victor Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Jules Deblauwe\", \"value\": \"3\", \"inputlabel\": \"Jules Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"John Atlassian\", \"value\": \"4\", \"inputlabel\": \"John Atlassian\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Steve Urkel\", \"value\": \"5\", \"inputlabel\": \"Steve Urkel\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Tim O&#39;hara\", \"value\": \"6\", \"inputlabel\": \"Tim O'hara\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"Ervin &quot;Magic&quot; Johnson\", \"value\": \"7\", \"inputlabel\": \"Ervin \\\"Magic\\\" Johnson\" }," + DatabaseValuesViewHelper.EOL +
									 "{ \"label\": \"admin Last\", \"value\": \"8\", \"inputlabel\": \"admin Last\" }" + DatabaseValuesViewHelper.EOL +
									 "]", s );
	}

	@Test
	public void testGetHtmlForAjaxResultsWith2ndLine() throws ClassNotFoundException
	{
		Class.forName( "org.hsqldb.jdbcDriver" );
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb" );
		addDemoData( parameters );


		parameters.setSqlQuery( "select id, firstname, lastname, city, country from customer" );
		parameters.setRenderingEditPattern( "{1} {2}<br/><i>{3}, {4}</i>" );
		AjaxViewHelper helper = new AjaxViewHelper( parameters, new DatabaseRowCache( parameters ), new DefaultVelocityManager() );
		String s = helper.getHtmlForAjaxResults( null, "TST", new MockI18nHelper(), AjaxViewResultsPurpose.EDIT );
		String sExpected = "[" + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Wim Deblauwe<br/><i>Heule, Belgium</i>\", \"value\": \"1\", \"inputlabel\": \"Wim Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Victor Deblauwe<br/><i>Heule, Belgium</i>\", \"value\": \"2\", \"inputlabel\": \"Victor Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Jules Deblauwe<br/><i>Heule, Belgium</i>\", \"value\": \"3\", \"inputlabel\": \"Jules Deblauwe\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"John Atlassian<br/><i>Sidney, Australia</i>\", \"value\": \"4\", \"inputlabel\": \"John Atlassian\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Steve Urkel<br/><i>Copenhagen, Denmark</i>\", \"value\": \"5\", \"inputlabel\": \"Steve Urkel\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Tim O&#39;hara<br/><i>Brussel, Belgium</i>\", \"value\": \"6\", \"inputlabel\": \"Tim O'hara\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"Ervin &quot;Magic&quot; Johnson<br/><i>Sidney, Australia</i>\", \"value\": \"7\", \"inputlabel\": \"Ervin \\\"Magic\\\" Johnson\" }," + DatabaseValuesViewHelper.EOL +
				 "{ \"label\": \"admin Last<br/><i>Brisbane, Australia</i>\", \"value\": \"8\", \"inputlabel\": \"admin Last\" }" + DatabaseValuesViewHelper.EOL +
				 "]";
		Assert.assertEquals(sExpected,  s );
	}

	@Test
	public void testGetHtmlForAjaxResultsNoResults() throws ClassNotFoundException
	{
		Class.forName( "org.hsqldb.jdbcDriver" );
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb" );
		addDemoData( parameters );


		parameters.setSqlQuery( "select id, firstname, lastname, city, country from customer where country is null" ); // query that does not match anything
		parameters.setRenderingEditPattern( "{1} {2}<br/><i>{3}, {4}</i>" );
		AjaxViewHelper helper = new AjaxViewHelper( parameters, new DatabaseRowCache( parameters ), new DefaultVelocityManager() );
		String s = helper.getHtmlForAjaxResults( null, "TST", new MockI18nHelper(), AjaxViewResultsPurpose.EDIT );
		Assert.assertEquals( "[" + DatabaseValuesViewHelper.EOL +  "]", s );
	}

	//@Test
	public void testGetAjaxHtmlForEdit() throws Exception
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setSqlQuery( "select id, firstname, lastname, city, country from customer" );
		AjaxViewHelper helper = new AjaxViewHelper( parameters, new DatabaseRowCache( parameters ), new DefaultVelocityManager() );
		String html = helper.getAjaxHtmlForEdit( "10000", null, "http://localhost:2990/jira", "TST", new MockI18nHelper() );
		Assert.assertEquals( "", html );
	}

	void addDemoData( DatabaseValuesCFParameters parameters )
	{
		Connection c = null;
		try
		{
			c = createConnection( parameters );

			Statement s = c.createStatement();

			s.executeUpdate( "CREATE TABLE customer ( id INTEGER IDENTITY, firstname VARCHAR(256), lastname VARCHAR(256), city VARCHAR(256), country VARCHAR(256), projectkey VARCHAR(256) )" );
			s.executeUpdate( "INSERT INTO customer VALUES( 1, 'Wim', 'Deblauwe', 'Heule', 'Belgium', 'TST' )" );
			s.executeUpdate( "INSERT INTO customer VALUES( 2, 'Victor', 'Deblauwe', 'Heule', 'Belgium', 'TST' )" );
			s.executeUpdate( "INSERT INTO customer VALUES( 3, 'Jules', 'Deblauwe', 'Heule', 'Belgium', 'PJT' )" );
			s.executeUpdate( "INSERT INTO customer VALUES( 4, 'John', 'Atlassian', 'Sidney', 'Australia', 'PJT')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 5, 'Steve', 'Urkel', 'Copenhagen', 'Denmark', 'TST')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 6, 'Tim', 'O''hara', 'Brussel', 'Belgium', 'PJT')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 7, 'Ervin', '\"Magic\" Johnson', 'Sidney', 'Australia', 'PJT')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 8, 'admin', 'Last', 'Brisbane', 'Australia', 'PJT')" );
		}
		catch (SQLException e)
		{
			//logger.error(e.getMessage(), e);
		}
		/*catch (GenericEntityException e)
		{
			//logger.error(e.getMessage(), e);
		}*/
		finally
		{
			closeConnection( c );
		}
	}

	private Connection createConnection( DatabaseValuesCFParameters parameters ) throws SQLException
	{
		return DriverManager.getConnection( parameters.getDatabaseConnectionUrl(),
											parameters.getDatabaseUser(),
											parameters.getDatabasePassword() );
	}

	private void closeConnection( Connection c )
	{
		if (c != null)
		{
			try
			{
				c.close();
			}
			catch (SQLException e)
			{
				//logger.error(e.getMessage(), e);
			}
		}
	}
}
