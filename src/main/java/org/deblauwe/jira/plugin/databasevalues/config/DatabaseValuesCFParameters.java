package org.deblauwe.jira.plugin.databasevalues.config;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class contains all the parameters for configuring the {@link org.deblauwe.jira.plugin.databasevalues.DatabaseValuesCFType}
 * <p/>
 * Created by Wim Deblauwe
 */
public class DatabaseValuesCFParameters
{
	private static final Logger logger = Logger.getLogger(DatabaseValuesCFParameters.class);

	public static final int EDIT_TYPE_COMBOBOX = 0;
	public static final int EDIT_TYPE_AJAX_INPUT = 1;
	public static final int EDIT_TYPE_CASCADING_SELECT = 2;

    public static final int SEARCH_TYPE_LIST = 0;
    public static final int SEARCH_TYPE_AJAX_INPUT = 1;

	private String m_databaseDriver;
	private String m_databaseConnectionUrl;
	private String m_databaseUser;
	private String m_databasePassword;
	private String m_sqlQuery;
	private String m_sqlLinkedFields;
	private Map<String,String> m_sqlSubstitutions = new HashMap<String, String>();
    private String m_sqlQuerySearch;
	private int m_primaryKeyColumnNumber;
	private String m_primaryKeyColumnName;
	private String m_renderingViewPattern;
	private String m_renderingEditPattern;
	private int m_renderingEditWidth;
	private int m_renderingSearchWidth;
	private String m_renderingSearchPattern;
	private long m_cacheTimeout;
	private int m_numberOfProjectsInCache;
	private int m_editType;
    private int m_searchType;
	private int m_groupingColumnNumber;
	private String m_sortingViewPattern;
	private String m_changeLogViewPattern;
	private String m_statisticsViewPattern;
	private Map<String,String> m_jqlQueries = new HashMap<String, String>();
	private Map<String,String> m_otherFields = new HashMap<String, String>();
	private boolean m_useInternalJiraDatabase = false;

	public String getDatabaseDriver()
	{
		return m_databaseDriver;
	}

	public void setDatabaseDriver( String databaseDriver )
	{
		m_databaseDriver = databaseDriver;
	}

	public String getDatabaseConnectionUrl()
	{
		return m_databaseConnectionUrl;
	}

	public void setDatabaseConnectionUrl( String databaseConnectionUrl )
	{
		m_databaseConnectionUrl = databaseConnectionUrl;
	}

	public String getDatabaseUser()
	{
		return m_databaseUser;
	}

	public void setDatabaseUser( String databaseUser )
	{
		m_databaseUser = databaseUser;
	}

	public String getDatabasePassword()
	{
		return m_databasePassword;
	}

	public void setDatabasePassword( String databasePassword )
	{
		m_databasePassword = databasePassword;
	}

	public String getSqlQuery()
	{
		return m_sqlQuery;
	}

	public void setSqlQuery( String sqlQuery )
	{
		m_sqlQuery = sqlQuery;
	}

    public String getSqlQuerySearch()
    {
        return m_sqlQuerySearch;
    }

    public void setSqlQuerySearch(String sqlQuerySearch)
    {
        m_sqlQuerySearch = sqlQuerySearch;
    }

    /**
	 * Get the number of the column that contains the primary key of the data. (0-based)
	 *
	 * @return column number
	 */
	public int getPrimaryKeyColumnNumber()
	{
		return m_primaryKeyColumnNumber;
	}

	/**
	 * Set the number of the column that contains the primary key of the data. (0-based)
	 *
	 * @param primaryKeyColumnNumber column number
	 */
	public void setPrimaryKeyColumnNumber( int primaryKeyColumnNumber )
	{
		m_primaryKeyColumnNumber = primaryKeyColumnNumber;
	}

	/**
	 * Gets the name of the primary key column
	 *
	 * @return
	 */
	public String getPrimaryKeyColumnName()
	{
		return m_primaryKeyColumnName;
	}

	/**
	 * Sets the name of the primary key column
	 *
	 * @param primaryKeyColumnName
	 */
	public void setPrimaryKeyColumnName( String primaryKeyColumnName )
	{
		m_primaryKeyColumnName = primaryKeyColumnName;
	}

	public String getRenderingViewPattern()
	{
		return m_renderingViewPattern;
	}

	public void setRenderingViewPattern( String renderingViewPattern )
	{
		m_renderingViewPattern = renderingViewPattern;
	}

	public String getRenderingEditPattern()
	{
		return m_renderingEditPattern;
	}

	public void setRenderingEditPattern( String renderingEditPattern )
	{
		m_renderingEditPattern = renderingEditPattern;
	}

	public int getRenderingEditWidth()
	{
		return m_renderingEditWidth;
	}

	public void setRenderingEditWidth( int renderingEditWidth )
	{
		m_renderingEditWidth = renderingEditWidth;
	}

	public int getRenderingSearchWidth()
	{
		return m_renderingSearchWidth;
	}

	public void setRenderingSearchWidth( int renderingSearchWidth )
	{
		m_renderingSearchWidth = renderingSearchWidth;
	}

	public String getRenderingSearchPattern()
	{
		return m_renderingSearchPattern;
	}

	public void setRenderingSearchPattern( String renderingSearchPattern )
	{
		m_renderingSearchPattern = renderingSearchPattern;
	}

	/**
	 * The timeout for the cache to expire
	 *
	 * @return the timeout in milliseconds
	 */
	public long getCacheTimeout()
	{
		return m_cacheTimeout;
	}

	/**
	 * The timeout for the cache to expire
	 *
	 * @param cacheTimeout the timeout in milliseconds
	 */
	public void setCacheTimeout( long cacheTimeout )
	{
		if (cacheTimeout < 0)
		{
			throw new IllegalArgumentException( "cacheTimeout cannot be negative" );
		}
		m_cacheTimeout = cacheTimeout;
	}

	/**
	 * Returns the maximum number of projects that should be kept in the cache.
	 *
	 * @return
	 */
	public int getNumberOfProjectsInCache()
	{
		return m_numberOfProjectsInCache;
	}

	/**
	 * Sets the number of projects that (maximum) will be held in the cache.
	 *
	 * @param numberOfProjectsInCache
	 */
	public void setNumberOfProjectsInCache( int numberOfProjectsInCache )
	{
		m_numberOfProjectsInCache = numberOfProjectsInCache;
	}

	/**
	 * Allows the user to choose between different types of inputting
	 * the value. Current choices:
	 * <ul>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_COMBOBOX}</li>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_AJAX_INPUT}</li>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_CASCADING_SELECT}</li>
	 * </ul>
	 *
	 * @return the int representing the edit type choice
	 */
	public int getEditType()
	{
		return m_editType;
	}

	/**
	 * Allows the user to choose between different types of inputting
	 * the value. Current choices:
	 * <ul>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_COMBOBOX}</li>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_AJAX_INPUT}</li>
	 * <li>{@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#EDIT_TYPE_CASCADING_SELECT}</li>
	 * </ul>
	 *
	 * @param editType the int representing the edit type choice
	 */
	public void setEditType( int editType )
	{
		if (editType != EDIT_TYPE_COMBOBOX
				&& editType != EDIT_TYPE_AJAX_INPUT
				&& editType != EDIT_TYPE_CASCADING_SELECT)
		{
			throw new IllegalArgumentException( "editType is unknown: " + editType );
		}
		m_editType = editType;
	}

    public int getSearchType() 
    {
        return m_searchType;
    }

    public void setSearchType(int searchType)
    {
        if (searchType != SEARCH_TYPE_LIST
                && searchType != SEARCH_TYPE_AJAX_INPUT)
        {
            throw new IllegalArgumentException("searchType is unknown: " + searchType);
        }
        m_searchType = searchType;
    }

    /**
	 * Get the column number to group the database values with.
	 * This is only relevant when using an edit type {@link #EDIT_TYPE_CASCADING_SELECT}.
	 *
	 * @return the number (0-based)
	 */
	public int getGroupingColumnNumber()
	{
		return m_groupingColumnNumber;
	}

	/**
	 * Set the column number to group the database values with.
	 * This is only relevant when using an edit type {@link #EDIT_TYPE_CASCADING_SELECT}.
	 *
	 * @param groupingColumnNumber the number (0-based)
	 */
	public void setGroupingColumnNumber( int groupingColumnNumber )
	{
		m_groupingColumnNumber = groupingColumnNumber;
	}

	/**
	 * The pattern that is used to sort the database values in the issue
	 * navigator by.
	 *
	 * @return the pattern to use
	 */
	public String getSortingViewPattern()
	{
		return m_sortingViewPattern;
	}

	/**
	 * The pattern that is used to sort the database values in the issue
	 * navigator by.
	 *
	 * @param sortingViewPattern the pattern to use
	 */
	public void setSortingViewPattern( String sortingViewPattern )
	{
		m_sortingViewPattern = sortingViewPattern;
	}

	public void setChangeLogViewPattern(String changeLogViewPattern)
	{
		m_changeLogViewPattern = changeLogViewPattern;
	}

	public String getChangeLogViewPattern()
	{
		return m_changeLogViewPattern;
	}

	/**
	 * Returns the pattern is used for rendering the database value in the pie chart,
	 * 2d filter statistics and single level group by report.
	 *
	 * @return the pattern to use
	 */
	public String getStatisticsViewPattern()
	{
		return m_statisticsViewPattern;
	}

	/**
	 * Sets the pattern is used for rendering the database value in the pie chart,
	 * 2d filter statistics and single level group by report.
	 *
	 * @param statisticsViewPattern the pattern to use
	 */
	public void setStatisticsViewPattern(String statisticsViewPattern)
	{
		m_statisticsViewPattern = statisticsViewPattern;
	}

	public Map<String, String> getSqlSubstitutions()
	{
		return m_sqlSubstitutions;
	}

	public void setSqlSubstitutions(Map<String, String> sqlSubstitutions)
	{
		logger.debug( "setting substitutions map: " + sqlSubstitutions );
		m_sqlSubstitutions = sqlSubstitutions;
	}

	public String doSqlSubstitutions()
	{
		String sqlQuery = getSqlQuery();
		Map<String,String> substitutions = getSqlSubstitutions();
		if( substitutions == null )
		{
			logger.warn( "substitutions are null" );
			return sqlQuery;
		}
		else
		{
			logger.debug( "Got " + substitutions.size() + " substitutions in map" );
		}

		for (String key : substitutions.keySet())
		{
			String value = substitutions.get(key);
			logger.debug("doing sql subst of '" + key + "' with '" + value + "'");

			 sqlQuery = sqlQuery.replaceAll("\\$\\{" + key + "\\}", value);
		}

		logger.debug( "sql query after substitutions: " + sqlQuery );

		return sqlQuery;
	}
	
	
	public void setOtherFields(Map<String, String> otherFields)
	{
		logger.debug( "Setting other fields: " + otherFields );
		m_otherFields = otherFields;
	}

	public Set<String> getOtherFieldIds()
	{
		return new TreeSet<String>(m_otherFields.keySet()); 
	}
	
	public String getOtherFieldPattern(String queryReference)
	{
		String value = m_otherFields.get(queryReference);
		return value;
	}

	public void setJqlQueries(Map<String, String> jqlQueries)
	{
		logger.debug( "Setting jql queries: " + jqlQueries );
		m_jqlQueries = jqlQueries;
	}

	public String getQuery(String queryReference, String queryValue)
	{
		String rawQuery = m_jqlQueries.get(queryReference);
		if( rawQuery != null )
		{
			return rawQuery.replace( "QUERY_VALUE", queryValue );
		}
		else
		{
			logger.warn( "Could not find a queryReference for '" + queryReference + "'" );
			return null;
		}
	}

	public boolean isExistingQueryReference(String queryReference)
	{
		return m_jqlQueries.containsKey( queryReference );
	}

	public Set<String> getQueryReferences()
	{
		return new TreeSet<String>(m_jqlQueries.keySet());  //To change body of created methods use File | Settings | File Templates.
	}

	public boolean isUseInternalJiraDatabase()
	{
		return m_useInternalJiraDatabase;
	}

	public void setUseInternalJiraDatabase(boolean useInternalJiraDatabase)
	{
		m_useInternalJiraDatabase = useInternalJiraDatabase;
	}
}
