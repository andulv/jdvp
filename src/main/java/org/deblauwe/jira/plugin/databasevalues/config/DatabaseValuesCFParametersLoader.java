package org.deblauwe.jira.plugin.databasevalues.config;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.issue.fields.CustomField;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseValuesCFParametersLoader
{
	private static final Logger logger = Logger.getLogger( DatabaseValuesCFParametersLoader.class );

	/**
	 * Cache is refreshed every 15 minutes by default
	 */
	static final long DEFAULT_CACHE_TIMEOUT = 15 * 60 * 1000;
	private static final int DEFAULT_NUMBER_OF_PROJECTS_IN_CACHE = 1;
	private static final int DEFAULT_EDITPATTERN_WIDTH = 50;
	private static final int DEFAULT_SEARCHPATTERN_WIDTH = 50;

	private DatabaseValuesCFParametersLoader()
	{
	}

	/**
	 * Loads the parameters for the {@link org.deblauwe.jira.plugin.databasevalues.DatabaseValuesCFType}
	 * from a properties file from disk.
	 *
	 * @param customField the customfield to load the properties of
	 * @return a {@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters} object.
	 *         Null if the properties could not be loaded.
	 */
	public static DatabaseValuesCFParameters loadParameters( CustomField customField )
	{
		DatabaseValuesCFParameters result;
		try
		{
			Properties properties = new Properties();
			InputStream stream = ClassLoaderUtils.getResourceAsStream( "jira-database-values-plugin-" + customField.getIdAsLong() +
					".properties", DatabaseValuesCFParametersLoader.class );

			if( stream == null )
			{
				URL resource = ClassLoaderUtils.getResource( "jira-database-values-plugin-" + customField.getIdAsLong() + ".properties", DatabaseValuesCFParametersLoader.class );
				if( resource != null )
				{
					File propFile = new File( resource.getFile() );
					stream = new FileInputStream( propFile );
				}
			}
			
			if (stream != null)
			{
				properties.load( stream );
				result = parametersFromProperties(properties);
			}
			else
			{
				logger.error("Could not load file 'jira-database-values-plugin-" + customField.getIdAsLong() + ".properties!");
				result = null;
			}
		}
		catch (IOException e)
		{
			logger.error( e.getMessage(), e );
			result = null;
		}

		return result;

	}

	private static DatabaseValuesCFParameters parametersFromProperties(Properties properties) {
		DatabaseValuesCFParameters result = new DatabaseValuesCFParameters();
		if (properties.containsKey("database.driver")
				&& properties.containsKey("database.user")
				&& properties.containsKey("database.password")
				&& properties.containsKey("database.connection.url"))
		{
			result.setDatabaseDriver(properties.getProperty("database.driver").trim());
			result.setDatabaseUser(properties.getProperty("database.user").trim());
			result.setDatabasePassword(properties.getProperty("database.password").trim());
			result.setDatabaseConnectionUrl(properties.getProperty("database.connection.url").trim());
		}
		else
		{
			result.setUseInternalJiraDatabase(true);
		}
		
		result.setSqlQuery( properties.getProperty( "sql.query" ).trim() );
		if( properties.getProperty( "sql.query.search" ) != null )
		{
			result.setSqlQuerySearch( properties.getProperty( "sql.query.search" ) );
		}
		else
		{
			result.setSqlQuerySearch( null );
		}
		result.setPrimaryKeyColumnNumber( Integer.parseInt( properties.getProperty( "primarykey.column.number" ).trim() ) );
		result.setRenderingViewPattern( properties.getProperty( "rendering.viewpattern" ).trim() );
		result.setRenderingEditPattern( properties.getProperty( "rendering.editpattern" ).trim() );
		if (properties.getProperty( "rendering.editwidth" ) != null)
		{
			result.setRenderingEditWidth( Integer.parseInt( properties.getProperty( "rendering.editwidth" ).trim() ) );
		}
		else
		{
			result.setRenderingEditWidth( DEFAULT_EDITPATTERN_WIDTH );
		}
		if (properties.getProperty( "rendering.searchwidth" ) != null)
		{
			result.setRenderingSearchWidth( Integer.parseInt( properties.getProperty( "rendering.searchwidth" ).trim() ) );
		}
		else
		{
			result.setRenderingSearchWidth( DEFAULT_SEARCHPATTERN_WIDTH );
		}
		result.setRenderingSearchPattern( properties.getProperty( "rendering.searchpattern" ).trim() );
		String changeLogViewPattern = properties.getProperty("rendering.changelog.viewpattern");
		if( changeLogViewPattern != null )
		{
			result.setChangeLogViewPattern( changeLogViewPattern.trim() );
		}
		else
		{
			result.setChangeLogViewPattern( result.getRenderingViewPattern() );
		}
		String statisticsViewPattern = properties.getProperty("rendering.statistics.viewpattern");
		if( statisticsViewPattern != null )
		{
			result.setStatisticsViewPattern( statisticsViewPattern.trim() );
		}
		else
		{
			result.setStatisticsViewPattern( result.getRenderingViewPattern() );
		}
		if (properties.getProperty( "cache.timeout" ) != null)
		{
			result.setCacheTimeout( Long.parseLong( properties.getProperty( "cache.timeout" ).trim() ) );
		}
		else
		{
			result.setCacheTimeout( DEFAULT_CACHE_TIMEOUT );
		}
		if (properties.getProperty( "cache.maximum.projects" ) != null)
		{
			result.setNumberOfProjectsInCache( Integer.parseInt( properties.getProperty( "cache.maximum.projects" ).trim() ) );
		}
		else
		{
			result.setNumberOfProjectsInCache( DEFAULT_NUMBER_OF_PROJECTS_IN_CACHE );
		}
		// This property is needed for sorting when you depend on the project key
		if (properties.getProperty( "primarykey.column.name" ) != null)
		{
			result.setPrimaryKeyColumnName( properties.getProperty( "primarykey.column.name" ).trim() );
		}

		if (properties.getProperty( "edit.type" ) != null)
		{
			if (Integer.parseInt( properties.getProperty( "edit.type" ).trim() ) == DatabaseValuesCFParameters.EDIT_TYPE_AJAX_INPUT)
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_AJAX_INPUT );
			}
			else if (Integer.parseInt( properties.getProperty( "edit.type" ).trim() ) == DatabaseValuesCFParameters.EDIT_TYPE_CASCADING_SELECT)
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_CASCADING_SELECT );
				result.setGroupingColumnNumber( Integer.parseInt( properties.getProperty( "rendering.editpattern.group.column.number" ).trim() ) );
			}
			else
			{
				result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX );
			}
		}
		else
		{
			result.setEditType( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX );
		}

		if( properties.getProperty( "search.type") != null )
		{
		    if( Integer.parseInt( properties.getProperty( "search.type" ).trim() ) == DatabaseValuesCFParameters.SEARCH_TYPE_AJAX_INPUT)
		    {
		        result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_AJAX_INPUT );
		    }
		    else
		    {
		        result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_LIST );
		    }
		}
		else
		{
		    result.setSearchType( DatabaseValuesCFParameters.SEARCH_TYPE_LIST );
		}

		if (properties.getProperty( "rendering.sortpattern" ) != null)
		{
			result.setSortingViewPattern( properties.getProperty( "rendering.sortpattern" ).trim() );
		}
		else
		{
			result.setSortingViewPattern( result.getRenderingViewPattern() );
		}

		if( properties.getProperty( "sql.linkedfields") != null )
		{
			logger.warn( "Linked fields are no longer supported!" );
		}

		result.setJqlQueries( loadJqlQueries(properties) );
		result.setOtherFields(loadOtherFields(properties));
		return result;
	}

	private static Map<String, String> loadJqlQueries(Properties properties)
	{
		int i = 1;

		Map<String, String> jqlQueries = new HashMap<String, String>();
		String queryReferencePropertyKey = "jql." + i + ".query.reference";
		String queryReferenceProperty = properties.getProperty(queryReferencePropertyKey);
		while( queryReferenceProperty != null )
		{
			String queryPropertyKey = "jql." + i + ".query";
			String queryProperty = properties.getProperty(queryPropertyKey);
			if( queryProperty != null )
			{
				jqlQueries.put( queryReferenceProperty, queryProperty );
			}
			else
			{
				logger.warn( "Found '" + queryReferencePropertyKey + "' but no matching query (Was looking for '" + queryPropertyKey +"')" );
			}
			i++;

			queryReferencePropertyKey = "jql." + i + ".query.reference";
			queryReferenceProperty = properties.getProperty(queryReferencePropertyKey);
		}

		return jqlQueries;
	}
	
	private static Map<String, String> loadOtherFields(Properties properties)
	{
		//.properties example
		//otherfield.1.id=10001
		//otherfield.1.textpattern={0} {1} {2}
		int i = 1;

		Map<String, String> retValue = new HashMap<String, String>();
		String propertyKeyId = "otherfield." + i + ".id";
		String propertyValueId = properties.getProperty(propertyKeyId);
		while( propertyValueId != null )
		{
			String propertyKeyPattern = "otherfield." + i + ".textpattern";
			String propertyValuePattern = properties.getProperty(propertyKeyPattern);
			if( propertyValuePattern != null )
			{
				retValue.put( propertyValueId, propertyValuePattern );
			}
			else
			{
				logger.warn( "Found '" + propertyKeyId + "' but no matching pattern (Was looking for '" + propertyValuePattern +"')" );
			}
			i++;

			propertyKeyId = "otherfield." + i + ".id";
			propertyValueId = properties.getProperty(propertyKeyId);
		}
		logger.debug("Loaded other fields. Count: " + retValue.size());

		return retValue;
	}
}
