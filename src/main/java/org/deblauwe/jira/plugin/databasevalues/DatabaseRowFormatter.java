package org.deblauwe.jira.plugin.databasevalues;

import com.atlassian.jira.util.I18nHelper;

import java.util.regex.Matcher;

public class DatabaseRowFormatter
{
	private DatabaseRowFormatter()
	{

	}

	public static String formatDatabaseRow(DatabaseRow databaseRow, String renderingPattern, I18nHelper i18nHelper )
	{
		String result = formatDatabaseRowLeaveQuotesAlone(databaseRow, renderingPattern, i18nHelper);
		result = result.replaceAll( "'", "&#39;" );

		return result;
	}
	
	public static String formatDatabaseRowLeaveQuotesAlone(DatabaseRow databaseRow, String renderingPattern, I18nHelper i18nHelper )
	{
		String result = renderingPattern;
		if (databaseRow != null)
		{
			for (int i = 0; i < databaseRow.getNumberOfValues(); i++)
			{
				Object value = databaseRow.getValue( i );
				if (value != null)
				{
					result = result.replaceAll( "\\{" + i + "(,[^}]*)?\\}", Matcher.quoteReplacement( value.toString().trim() ) );
				}
				else
				{
					result = result.replaceAll( "\\{" + i + "(,([^}]*))?\\}", "$2" );
				}
			}
		}
		return result;
	}

}
