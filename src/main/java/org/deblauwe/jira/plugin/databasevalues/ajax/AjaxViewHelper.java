package org.deblauwe.jira.plugin.databasevalues.ajax;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.velocity.VelocityManager;

import org.apache.log4j.Logger;
import org.deblauwe.jira.plugin.databasevalues.DatabaseRow;
import org.deblauwe.jira.plugin.databasevalues.DatabaseRowCache;
import org.deblauwe.jira.plugin.databasevalues.DatabaseRowCachePurpose;
import org.deblauwe.jira.plugin.databasevalues.DatabaseRowFormatter;
import org.deblauwe.jira.plugin.databasevalues.DatabaseValuesViewHelper;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class generates the HTML for all things related to AJAX.
 */
public class AjaxViewHelper
{
// ------------------------------ FIELDS ------------------------------

	private static final Logger logger = Logger.getLogger( AjaxViewHelper.class );

	private static final String DIRNAME_PATH = "templates/";
	private static final String FILENAME = "autocomplete-input.vm";

	private final DatabaseValuesCFParameters m_parameters;
	private final DatabaseRowCache m_databaseRowCache;
	private final VelocityManager m_velocityManager;

// --------------------------- CONSTRUCTORS ---------------------------

	public AjaxViewHelper( DatabaseValuesCFParameters parameters, DatabaseRowCache databaseRowCache, VelocityManager velocityManager )
	{
		m_parameters = parameters;
		m_databaseRowCache = databaseRowCache;
		m_velocityManager = velocityManager;
	}

// -------------------------- PUBLIC METHODS --------------------------

	/**
	 * Returns the html that will be used by the ajax auto-completer from scriptacolous.
	 *
	 * @param query      the string the user already typed. Can be null.
	 * @param projectKey the JIRA key of the current project. Can be null.
	 * @param i18nHelper helper for internationalization
	 * @return HTML snippet
	 */
	public String getHtmlForAjaxResults( String query, String projectKey, I18nHelper i18nHelper, AjaxViewResultsPurpose purpose )
	{
		DatabaseRowCachePurpose databaseRowCachePurpose;
		String renderingPattern;
		if (purpose.equals( AjaxViewResultsPurpose.EDIT ))
		{
			renderingPattern = m_parameters.getRenderingEditPattern();
			databaseRowCachePurpose = DatabaseRowCachePurpose.EDIT;
		}
		else if (purpose.equals( AjaxViewResultsPurpose.SEARCH ))
		{
			renderingPattern = m_parameters.getRenderingSearchPattern();
			databaseRowCachePurpose = DatabaseRowCachePurpose.SEARCH;
		}
		else
		{
			throw new IllegalArgumentException( "purpose is unknown: " + purpose );
		}
		return getHtmlForAjaxResultsWithRenderingPattern( query, projectKey, i18nHelper, renderingPattern, databaseRowCachePurpose );
	}

	/**
	 * Returns the HTML that should be used for AJAX-style editing the value of the custom field.
	 *
	 * @param customFieldId the id of the custom field
	 * @param id            the primary key of the data (as determined by {@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#getPrimaryKeyColumnNumber()})
	 * @param baseurl       the base url of the JIRA installation
	 * @param projectKey    the JIRA key of the current project. Can be null.
	 * @param i18nHelper    helper for internationalization
	 * @return a HTML snippet
	 * @see http://metapundit.net/sections/blog/ajax_autocomplete_with_scriptaculous
	 */
	public String getAjaxHtmlForEdit( String customFieldId, String id, String baseurl, String projectKey, I18nHelper i18nHelper )
	{
		AjaxViewResultsPurpose ajaxViewResultsPurpose = AjaxViewResultsPurpose.EDIT;

		StringBuffer builder = buildJavascriptForAutocomplete( customFieldId, id, baseurl, projectKey, i18nHelper, ajaxViewResultsPurpose );
		return builder.toString();
	}

	/**
	 * Returns the HTML that should be used for AJAX-style editing the value of the custom field.
	 *
	 * @param customFieldId the id of the custom field
	 * @param id            the primary key of the data (as determined by {@link org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters#getPrimaryKeyColumnNumber()})
	 * @param baseurl       the base url of the JIRA installation
	 * @param projectKey    the JIRA key of the current project. Can be null.
	 * @param i18nHelper    helper for internationalization
	 * @return a HTML snippet
	 * @see http://metapundit.net/sections/blog/ajax_autocomplete_with_scriptaculous
	 */
	public String getAjaxHtmlForSearch( String customFieldId, String id, String baseurl, String projectKey, I18nHelper i18nHelper )
	{
		return buildJavascriptForAutocomplete( customFieldId, id, baseurl, projectKey, i18nHelper, AjaxViewResultsPurpose.SEARCH ).toString();
	}

// -------------------------- PRIVATE METHODS --------------------------

	private StringBuffer buildJavascriptForAutocomplete( String customFieldId, String id, String baseurl, String projectKey, I18nHelper i18nHelper, AjaxViewResultsPurpose ajaxViewResultsPurpose )
	{
		StringBuffer builder = new StringBuffer();

		String valueString = getValueStringForAjaxInputField( id, i18nHelper, projectKey );

		Map<String, Object> context = new HashMap<String, Object>();
		context.put( "baseurl", baseurl );
		context.put( "customFieldId", customFieldId );
		context.put( "renderingEditWidth", m_parameters.getRenderingEditWidth() );
		context.put( "projectKey", projectKey );
		context.put( "ajaxPurpose", ajaxViewResultsPurpose );
		context.put( "valueId", id ); //The id in the database
		context.put( "valueString", valueString ); //The string to render to the user

		ApplicationProperties applicationProperties = ComponentManager.getInstance().getApplicationProperties();
		String baseUrl = applicationProperties.getString( APKeys.JIRA_BASEURL );
		String webworkEncoding = applicationProperties.getString( APKeys.JIRA_WEBWORK_ENCODING );

		String renderedText = m_velocityManager.getEncodedBody( DIRNAME_PATH, FILENAME, baseUrl, webworkEncoding, context );
		builder.append( renderedText );

		return builder;
	}

	private String getValueStringForAjaxInputField( String id, I18nHelper i18nHelper, String projectKey )
	{
		String valueString;
		if (id != null)
		{
			DatabaseRow databaseRow = m_databaseRowCache.getProjectSpecificCacheMap( projectKey ).get( id );
			valueString = DatabaseRowFormatter.formatDatabaseRow( databaseRow, m_parameters.getRenderingEditPattern(), i18nHelper );
			valueString = stripExtraInfo( valueString );
		}
		else
		{
			valueString = "";
		}
		return valueString;
	}

	private String getHtmlForAjaxResultsWithRenderingPattern( String query, String projectKey, I18nHelper i18nHelper, String renderingPattern, DatabaseRowCachePurpose databaseRowCachePurpose )
	{
		StringBuffer builder = new StringBuffer( "[" );
		builder.append( DatabaseValuesViewHelper.EOL );
		String lowerCaseQuery;
		if (query != null)
		{
			lowerCaseQuery = query.toLowerCase();
			logger.debug( "Query: " + lowerCaseQuery );
		}
		else
		{
			lowerCaseQuery = null;
		}
		Collection<DatabaseRow> databaseRows = m_databaseRowCache.getDatabaseRows( projectKey, databaseRowCachePurpose );
		for (DatabaseRow databaseRow : databaseRows)
		{
			String databaseRowId = databaseRow.getValue( m_parameters.getPrimaryKeyColumnNumber() ).toString();
			String editString = DatabaseRowFormatter.formatDatabaseRow( databaseRow, renderingPattern, i18nHelper );
			if (lowerCaseQuery == null
					|| editString.toLowerCase().indexOf( lowerCaseQuery ) != -1)
			{
				builder.append( "{ \"label\": \"" );
				builder.append( editString.replaceAll( "\"", "&quot;" ) );
				builder.append( "\", \"value\": \"" );
				builder.append( databaseRowId );
				builder.append( "\", \"inputlabel\": \"" );
				builder.append( stripExtraInfo( editString ) );

				builder.append( "\" }," );
				builder.append( DatabaseValuesViewHelper.EOL );
			}
			else
			{
				logger.debug( "Not adding row to JSON (lowerCaseQuery=" + lowerCaseQuery +
									  ", editString=" + editString +
									  "): " + databaseRow );
			}
		}

		if (builder.length() > ("[" + DatabaseValuesViewHelper.EOL).length())
		{
			builder.deleteCharAt( builder.length() - DatabaseValuesViewHelper.EOL.length() - 1 );
		}
		builder.append( "]" );
		String s = builder.toString();
		logger.debug( "Returning JSON: " + s );
		return s;
	}

	private String stripExtraInfo( String editString )
	{
		if (editString.contains( "<br/>" ))
		{
			return editString.substring( 0, editString.indexOf( "<br/>" ) ).replaceAll( "\"", "\\\\\"" ).replaceAll( "&#39;", "'" );
		}
		else
		{
			return editString.replaceAll( "&#39;", "'" ).replaceAll( "\"", "\\\\\"" );
		}
	}
}
