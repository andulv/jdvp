package org.deblauwe.jira.plugin.databasevalues;

import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;

public class OtherFieldsHelper {

	public static void setOrUpdateCustomFieldById(Issue issue, long customFieldId, String newContent)
	{
		CustomFieldManager customFieldManager= ComponentAccessor.getCustomFieldManager();
		CustomField cf = customFieldManager.getCustomFieldObject(customFieldId);
		setOrUpdateCustomField(issue, newContent, cf);
	}
	
	public static void setOrUpdateCustomFieldByName(Issue issue, String customFieldName, String newContent)
	{
		CustomFieldManager customFieldManager= ComponentAccessor.getCustomFieldManager();
		CustomField cf = customFieldManager.getCustomFieldObjectByName(customFieldName);
		
		setOrUpdateCustomField(issue, newContent, cf);	
	}

	public static void setOrUpdateCustomField(Issue issue, String newContent, CustomField cf) 
	{
		Object fieldValue=issue.getCustomFieldValue(cf);
		String newValue="";
		
		if(fieldValue==null)
		{
			newValue = newContent;
		}
		else
		{			
			newValue = fieldValue.toString();
			if(!newValue.contains(newContent))
			{
				newValue = newValue + " " + newContent;
			}			 
		}
			
		IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
		ModifiedValue modValue=new ModifiedValue(fieldValue, newValue);
		cf.updateValue(null,issue, modValue, changeHolder);
	}

	
}
