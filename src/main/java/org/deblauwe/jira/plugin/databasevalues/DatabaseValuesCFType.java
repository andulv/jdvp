package org.deblauwe.jira.plugin.databasevalues;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

import com.atlassian.velocity.VelocityManager;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParametersLoader;
import org.apache.log4j.Logger;

public class DatabaseValuesCFType extends TextCFType implements SortableCustomField<String>/*implements SortableCustomField*//*implements MultipleSettableCustomFieldType,
		MultipleCustomFieldType,
		SortableCustomField*/
{

	// ------------------------------ FIELDS ------------------------------
	private static final Logger logger = Logger.getLogger( DatabaseValuesCFType.class );
	//private OptionsManager m_optionsManager;
	private Map/*<Long, DatabaseValuesViewHelper>*/ _helpersCache = new HashMap();
	private VelocityManager m_velocityManager;

// --------------------------- CONSTRUCTORS ---------------------------

	public DatabaseValuesCFType(CustomFieldValuePersister customFieldValuePersister,
								GenericConfigManager genericConfigManager,
								VelocityManager velocityManager )
	{
		super(customFieldValuePersister, genericConfigManager);
		m_velocityManager = velocityManager;
	}
	

// ------------------------ INTERCEPT UPDATES TO THIS FIELD - USED FOR UPDATING OTHER FIELDS ------------------------	
	@Override
	public void createValue(CustomField field, Issue issue, Object value) {
		logger.debug("DatabaseValuesCFType.createValue, value: " + value.toString());
    	DatabaseValuesViewHelper helper=getDatabaseValuesViewHelper(field);
		int updatedFields=helper.updateOtherFields(issue,value.toString(),  getI18nBean());
		logger.debug("DatabaseValuesCFType.createValue, updated fields: " + updatedFields);
		super.createValue(field, issue, value);
	}
    
    @Override
    public void updateValue(CustomField customField, Issue issue, Object value) {
		logger.debug("DatabaseValuesCFType.updateValue, value: " + value.toString());
    	DatabaseValuesViewHelper helper=getDatabaseValuesViewHelper(customField);
    	int updatedFields=helper.updateOtherFields(issue,value.toString(), getI18nBean());
		logger.debug("DatabaseValuesCFType.updateValue, updated fields: " + updatedFields);
		super.updateValue(customField, issue, value);
    }
	
	
// ------------------------ INTERFACE METHODS ------------------------

// --------------------- Interface CustomFieldType ---------------------

	protected DatabaseValuesViewHelper getDatabaseValuesViewHelper(CustomField customField) {
		DatabaseValuesViewHelper fromCache = (DatabaseValuesViewHelper)_helpersCache.get( customField.getIdAsLong() );
		if (fromCache == null)
		{
			DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( customField );
			if (parameters != null)
			{
				fromCache = new DatabaseValuesViewHelper( parameters, m_velocityManager );
				_helpersCache.put( customField.getIdAsLong(), fromCache );
			}
			else
			{
				logger.error( "Parameters for custom field with id '" + customField.getIdAsLong() + "' could not be loaded!" );
			}
		}
		
		return fromCache;
	}
	
	@Override
	public Map getVelocityParameters( Issue issue, CustomField customField, FieldLayoutItem fieldLayoutItem )
	{
		Map result = super.getVelocityParameters( issue, customField, fieldLayoutItem );
		result.put( "databaseValuesViewHelper", getDatabaseValuesViewHelper( customField ) );
		return result;
	}

	// TODO: Enable this again when configuring via webinterface works
//
//	public List getConfigurationItemTypes()
//	{
//		final List configurationItemTypes = EasyList.build( JiraUtils.loadComponent( DefaultValueConfigItem.class ) );
//		configurationItemTypes.add( new DatabaseValuesCFConfigItemType( m_optionsManager ) );
//		return configurationItemTypes;
//	}

// --------------------- Interface MultipleCustomFieldType ---------------------

//	public Options getOptions( FieldConfig fieldConfig, JiraContextNode jiraContextNode )
//	{
//		return m_optionsManager.getOptions( fieldConfig );
//	}

// --------------------- Interface MultipleSettableCustomFieldType ---------------------

//	public Set getIssueIdsWithValue( CustomField customField, Option option )
//	{
//		if (option != null)
//		{
//			return customFieldValuePersister.getIssueIdsWithValue( customField, PersistenceFieldType.TYPE_LIMITED_TEXT, option.getValue() );
//		}
//		else
//		{
//			return Collections.EMPTY_SET;
//		}
//	}

//	public void removeValue( CustomField customField, Issue issue, Option option )
//	{
//		updateValue( customField, issue, null );
//	}


	@Override
	public String getChangelogValue(CustomField field, Object value)
	{
		DatabaseValuesViewHelper helper = getDatabaseValuesViewHelper(field);
		if( helper != null )
		{
			return helper.getTextForChangeLog((String)value, getI18nBean() );
		}
		else
		{
			return super.getChangelogValue(field, value);
		}
	}

	@Override
	public int compare(String customFieldObjectValue1, String customFieldObjectValue2, FieldConfig fieldConfig)
	{
		// This compare function is only used when no searcher has been configured with the custom field
		// If there is a searcher, the sorting is done via DatabaseValuesSearcher
		DatabaseValuesViewHelper helper = getDatabaseValuesViewHelper(fieldConfig.getCustomField());

		return super.compare(helper.getStringForSorting(customFieldObjectValue1),
				helper.getStringForSorting(customFieldObjectValue2),
				fieldConfig);	//To change body of overridden methods use File | Settings | File Templates.
	}
}




